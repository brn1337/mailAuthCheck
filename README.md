# Mail Authentication Checker ![](img/logo-small.png)

> Inspired by [spoofcheck](https://github.com/BishopFox/spoofcheck).

Mail Authentication Checker verify SPF, DKIM, DMARC and Root Domain hack to determine the security of a domain against spoofing/phishing attacks.

Features:

- Check SPF recursively
- Bruteforce DKIM selector
- Check DMARC record
- Check Root Domain hack

# Usage

```bash
bash mailAuthCheck.sh google.com
```

# Demo

Both recordings below were slowed to improve readability, in reality it took less than 1 second to execute.

![](img/demo1.gif)

![](img/demo2.gif)

# Installation

```bash
git clone https://gitlab.com/brn1337/mailAuthCheck.git
cd mailAuthCheck
```

# License

Mail Authentication Checker is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)

```
CC-BY-NC-SA This license requires that reusers give credit to the creator.
It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only.
If others modify or adapt the material, they must license the modified material under identical terms.
- Credit must be given to you, the creator.
- Only noncommercial use of your work is permitted.
- Adaptations must be shared under the same terms.
```
