#!/usr/bin/env bash

echo -e "\nMail Authentication Checker v1.0"

if [ $# -ne 1 ]; then
    echo "Usage: $0 DOMAIN"
    exit
fi

echo -e "\nAnalysing ${1}"
echo "Note: the existence of a security record does not mean it is actually implemented"
echo 'Test AV: X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
echo 'Test Antispam: XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X'

root=$(echo ${1}|cut -d. -f1)
DNS_SERVER="@8.8.8.8"
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
RESET="\033[0m"
BOLD="\033[1m"
GREY="\e[90m"



###########################
########    SPF    ########
###########################
# https://dmarcian.com/spf-syntax-table/

function spf_check {
    if [ $# -eq 1 ]; then
        local pre=""
        local detail=""
    elif [ $# -eq 2 ]; then
        local pre="${2}"
        local detail=" (${1})"
    fi
    local spf=$(dig ${DNS_SERVER} +tries=5 +short TXT ${1} | grep -i 'v=spf1' | sed "s/\" \"//g")
    local spf_rate="${GREY}undefined"
    if [ -z "$spf" ]; then
        spf_rate="${RED}missing"
    elif [[ $spf == *"-all"* ]]; then
        spf_rate="${GREEN}secure"
    elif [[ $spf == *"~all"* ]]; then
        spf_rate="${YELLOW}partially secure"
    elif [[ $spf == *"+all"* ]]; then
        spf_rate="${RED}insecure"
    elif [[ $spf == *"?all"* ]]; then
        if [ $# -eq 1 ]; then
            spf_rate="${RED}insecure"
        else
            spf_rate="${YELLOW}partially secure"
        fi
    fi
    echo -e "\n${pre}${BOLD}SPF${RESET}${detail}: ${spf_rate}${RESET}"
    if [ ! -z "$spf" ]; then echo "${pre}$spf"; fi
    local spf_recur=$(echo $spf | grep -oE "(redirect=|include:)[^ \"]*" | sed "s/include://" | sed "s/redirect=//" | sort | uniq)
    for spf_sub in ${spf_recur}; do
        spf_check ${spf_sub} "${pre}    "
    done
}
spf_check ${1}



###########################
########    DKIM   ########
###########################

dkim_rate="${GREY}undefined"
dkim_detail=""
for selector in ${root} default dkim dkim-shared dkimpal email gamma google mail mdaemon selector selector1 selector2 selector3 selector4 selector5
do
    dkim=$(dig ${DNS_SERVER} +tries=5 TXT ${selector}._domainkey.${1} | grep -i 'v=DKIM' | sed "s/\" \"//g")
    if [ ! -z "$dkim" ]; then
        dkim_rate="${GREEN}secure"
        dkim_detail=" (${selector}._domainkey.${1})"
        break
    fi
done
if [[ $dkim_rate == "${GREY}undefined" ]]; then
    dkim_rate="${YELLOW}not found"
fi
echo -e "\n${BOLD}DKIM${RESET}${dkim_detail}: ${dkim_rate}${RESET}"
if [ ! -z "$dkim" ]; then echo "$dkim"; fi



###########################
########   DMARC   ########
###########################

dmarc=$(dig ${DNS_SERVER} +tries=5 +short TXT _dmarc.${1} | grep -i 'v=DMARC' | sed "s/\" \"//g")
dmarc_rate="${GREY}undefined"
if [ -z "$dmarc" ]; then
    dmarc_rate="${RED}missing"
elif [[ $dmarc == *"p=reject"* ]]; then
    dmarc_rate="${GREEN}secure"
elif [[ $dmarc == *"p=quarantine"* ]]; then
    dmarc_rate="${GREEN}secure"
elif [[ $dmarc == *"p=none"* ]]; then
    dmarc_rate="${RED}insecure"
fi
echo -e "\n${BOLD}DMARC${RESET}: ${dmarc_rate}${RESET}"
if [ ! -z "$dmarc" ]; then echo "$dmarc"; fi



###########################
#####   ROOT DOMAIN   #####
###########################
# https://www.avanan.com/blog/office-365-and-gmail-root-domain-exploit

rootmicro=$(dig ${DNS_SERVER} +tries=5 +short MX ${root}.onmicrosoft.com)
rootgoog=$(dig ${DNS_SERVER} +tries=5 +short MX ${1} | grep -Ei "\.goog\.$|\.google\.com\.")
roothack_rate="${GREY}undefined"
if [ ! -z "$rootmicro" ]; then
    roothack_rate="${YELLOW}potentially vulnerable"
elif [ ! -z "$rootgoog" ]; then
    roothack_rate="${YELLOW}potentially vulnerable"
else
    roothack_rate="${GREEN}secure"
fi
echo -e "\n${BOLD}Root Domain Hack${RESET}: ${roothack_rate}${RESET}"
if [ ! -z "$rootmicro" ]; then echo "$rootmicro"; echo -e "    try user@${root}.onmicrosoft.com"; fi
if [ ! -z "$rootgoog" ]; then echo "$rootgoog"; echo -e "    try user@${1}.test-google-a.com"; fi



echo
